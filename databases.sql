-- create databases
CREATE DATABASE IF NOT EXISTS `appdb`;
CREATE DATABASE IF NOT EXISTS `testingdb`;

-- create root users and grants
-- CREATE USER 'root'@'localhost' IDENTIFIED BY 'myrootpassword';
-- GRANT ALL ON *.* TO 'root'@'%';

create user 'user'@'localhost' identified by 'myuserpassword';
grant all on *.* to 'user'@'%';