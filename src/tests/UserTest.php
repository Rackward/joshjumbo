<?php

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    public function test_can_login()
    {
        $user = factory( User::class )->create( [
            "username" => "josh",
            "password" => User::hashPassword( "lmao" ),
        ] );

        $response = $this->json( "GET", "/user/login?username=josh&password=lmao" );

        // $response->assertStatus( 200 );

        $this->assertEquals( 200, $this->response->getStatusCode() );
    }

    public function test_invalid_username_password()
    {
        $user = factory( User::class )->create();

        $response = $this->json( "GET", "/user/login?username=invalid&password=invalid" );

        // $response->assertStatus( 400 );

        $this->assertEquals( 400, $this->response->getStatusCode() );
    }
}