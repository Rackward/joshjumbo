<?php

use App\Models\Pet;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Laravel\Lumen\Testing\WithoutMiddleware;

class PetTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_can_show_pet()
    {
        $pet = factory( Pet::class )->create();

        $response = $this->json( "GET", "/pet/{$pet->id}", [] );

        $this->assertEquals( 200, $this->response->getStatusCode() );
    }

    public function test_pet_show_invalid_url_parameter()
    {
        $response = $this->json( "GET", "/pet/a", [] );
            // ->seeJson( [
            //     "code" => 400,
            //     "message" => "Invalid ID supplied",
            // ] );

        $this->assertEquals( 400, $this->response->getStatusCode() );

        // $response->assertStatus( 400 );
    }

    public function test_pet_not_found()
    {
        $response = $this->json( "GET", "/pet/1", [] );
            // ->seeJson( [
            //     "code" => 404,
            //     "message" => "Pet not found"
            // ] );

        $this->assertEquals( 404, $this->response->getStatusCode() );

        // $response->assertStatus( 404 );        
    }
}
