<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( "pets", function ( Blueprint $table ) {
            $table->bigInteger( "id" )->autoIncrement();
            $table->bigInteger( "category_id" );
            $table->string( "name" );
            $table->enum( "status", ["available", "pending", "sold"] );
            $table->foreign( "category_id" )->references( "id" )->on( "categories" );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( "pets" );
    }
}
