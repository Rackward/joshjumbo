<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( "orders", function ( Blueprint $table ) {
            $table->bigInteger( "id" )->autoIncrement();
            $table->bigInteger( "pet_id" );
            $table->integer( "quantity" );
            $table->string( "shipDate" );
            $table->enum( "status", ["placed", "approved", "delivered"] );
            $table->boolean( "complete" );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( "orders" );
    }
}
