<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( "pet_tags", function ( Blueprint $table ) {
            $table->bigInteger( "pet_id" );
            $table->bigInteger( "tag_id" );
            $table->primary( ["pet_id", "tag_id"] );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( "pet_tags" );
    }
}
