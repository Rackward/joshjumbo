<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Models\Category;
use App\Models\Pet;
use App\Models\Tag;
use App\Models\User;

$factory->define( Pet::class, function ( Faker\Generator $faker )
{
    return [
        "category_id" => Category::create( ["name" => $faker->word] )->id,
        "name" => $faker->firstName,
        // "photoUrls" => [],
        // "tags" => [],
        "status" => $faker->randomElement( ["available", "pending", "sold"] ),
    ];
} );

$factory->afterCreating( Pet::class, function( $pet, $faker )
{
    $numberOfTags = $faker->numberBetween( 0, 5 );

    $tags = Tag::all();

    $faker->unique( true );

    if ( $tags->count() > 0 )
    {
        for ( $i = 0; $i < $numberOfTags; ++$i )
        {
            $tag = $faker->unique()->randomElement( $tags );
            $pet->tags()->attach( $tag );
        }
    }

    $pet->save();

    return;
} );

$factory->define( User::class, function( Faker\Generator $faker )
{
    return [
        "username" => $faker->domainWord,
        "firstName" => $faker->firstName,
        "lastName" => $faker->lastName,
        "email" => $faker->email,
        "password" => User::hashPassword( $faker->password ),
        "phone" => $faker->phoneNumber,
        "userStatus" => $faker->randomNumber,
    ];
} );