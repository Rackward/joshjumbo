<?php

use App\Models\Category;
use App\Models\Pet;
use App\Models\Tag;
use App\Models\PetTag;
use Illuminate\Database\Seeder;
use Faker\Factory;

class PetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PetTag::truncate();
        Pet::truncate();

        factory( Pet::class, 10 )->create();

        return;
    }
}
