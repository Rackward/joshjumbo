<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Factory;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        factory( User::class )->create( [
            "username" => "josh",
            "password" => User::hashPassword( "lmao" ),
        ] );
        
        factory( User::class, 10 )->create();

        return;
    }
}
