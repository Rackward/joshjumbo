<?php

use App\Models\Tag;
use Illuminate\Database\Seeder;
use Faker\Factory;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::truncate();

        $faker = Factory::create();

        for( $i = 0; $i < 20; ++$i )
        {
            Tag::create( [
                "name" => $faker->unique()->word,
            ] );
        }

        return;
    }
}
