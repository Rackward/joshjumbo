<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get( "/user/login", "UserController@login" );

$router->group( ["middleware" => "auth"], function () use ( $router )
{
    $router->get( "/pet/findByStatus", "PetController@findByStatus" );
    $router->get( "/pet/{petId}", "PetController@show" );
    $router->post( "/pet/{petId}", "PetController@update" );
    $router->delete( "/pet/{petId}", "PetController@destroy" );
    $router->post( "/pet/{petId}/uploadImage", "PetController@uploadImage" );
    $router->post( "/pet", "PetController@store" );
    $router->put( "/pet", "PetController@update" );

    $router->get( "/store/inventory", "StoreController@index" );
    $router->get( "/store/order/{orderId}", "StoreController@show" );
    $router->delete( "/store/order/{orderId}", "StoreController@destroy" );
    $router->post( "/store/order", "StoreController@store" );

    $router->get( "/user/logout", "UserController@logout" );
    $router->post( "/user/createWithArray", "UserController@storeByArray" );
    $router->post( "/user/createWithList", "UserController@storeByList" );
    $router->get( "/user/{username}", "UserController@show" );
    $router->put( "/user/{username}", "UserController@update" );
    $router->delete( "/user/{username}", "UserController@destroy" );
    $router->post( "/user", "UserController@store" );
} );
