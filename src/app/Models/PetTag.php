<?php
namespace App\Models;

use App\Models\Pet;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Model;

class PetTag extends Model
{
    public function pet()
    {
        return $this->belongsTo( Pet::class );
    }

    public function tag()
    {
        return $this->belongsTo( Tag::class );
    }
}
