<?php
namespace App\Models;

use App\Models\Pet;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        "pet_id", "quantity", "shipDate", "status", "complete"
    ];

    public function pet()
    {
        return $this->belongsTo( Pet::class );
    }
}