<?php
namespace App\Models;

use App\Models\Pet;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $visible = [
        "id",
        "name",
    ];

    protected $fillable = [
        "name",
    ];

    public function pet()
    {
        return $this->belongsTo( Pet::class );
    }
}