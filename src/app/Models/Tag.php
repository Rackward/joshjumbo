<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $visible = [
        "id",
        "name"
    ];

    public function petTags()
    {
        return $this->hasMany( PetTag::class );
    }

    public function pets()
    {
        return $this->belongsToMany( Pet::class, "pet_tags" );
    }
}
