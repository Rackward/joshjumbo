<?php
namespace App\Models;

use App\Models\Category;
use App\Models\Tag;
use App\Models\Order;
use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    protected $hidden = [
        "category_id", "created_at", "updated_at",
    ];

    protected $fillable = [
        "name", "category_id", "status"
    ];

    protected $appends = [
        "photoUrls"
    ];

    public function category()
    {
        return $this->hasOne( Category::class, "id", "category_id" );
    }

    public function tags()
    {
        return $this->belongsToMany( Tag::class, "pet_tags" );
    }

    public function orders()
    {
        return $this->hasMany( Order::class );
    }

    public function getPhotoUrlsAttribute()
    {
        // TODO actually look in file storage for photo urls
        return [];
    }
}
