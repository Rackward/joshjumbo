<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "username", "firstName", "lastName", "email", "phone", "userStatus"
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', "api_token", "api_expires",
    ];

    /**
     * @param string $a_password
     * @return string
     */
    public static function hashPassword( $a_password )
    {
        return hash( "sha256", $a_password . "thisisasalt" );
    }

    /**
     * Generates a new token and expiration time.
     * Must save the model after calling this function to register changes in the db
     * 
     * @return User
     */
    public function generateNewToken()
    {
        $dt = new \DateTime();
        $dt->add( new \DateInterval( "PT30M" ) );
        $expiration = $dt->format( "Y-m-d H:i:s" );

        $this->api_token = Str::random(80);
        $this->api_expires = $expiration;

        return $this;
    }

    public function getApiToken()
    {
        return $this->api_token;
    }

    public function getApiExpires()
    {
        return $this->api_expires;
    }
}
