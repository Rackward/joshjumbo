<?php
namespace App\Http\Controllers;

use App\Models\Pet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PetController extends Controller
{
    public function show( Request $request, $petId )
    {
        $validator = Validator::make( [
            "petId" => $petId,
        ], [
            "petId" => "required|integer"
        ] );

        if ( $validator->fails() )
        {
            abort( 400, "Invalid ID supplied" );
            // return response()->json( "Invalid ID supplied", 400 );
        }

        $pet = Pet::with( ["tags", "category"] )->find( $petId ) ?? abort( 404, "Pet not found" );

        return $pet;
    }

    public function update( Request $request, $petId )
    {
        abort( 501, "Not Implemented" );
    }

    public function destroy( Request $request, $petId )
    {
        abort( 501, "Not Implemented" );
    }

    public function uploadImage( Request $request, $petId )
    {
        abort( 501, "Not Implemented" );
    }

    public function store( Request $request )
    {
        abort( 501, "Not Implemented" );
    }
    
    public function findByStatus( Request $request )
    {
        abort( 501, "Not Implemented" );
    }
}