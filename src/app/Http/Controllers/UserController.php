<?php
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function login( Request $request )
    {
        // The spec clearly says to pass username / password and query params in clear text
        // This is would obviously not be a production login mechanism as it has multiple security flaws, even when using SSL
        $validator = Validator::make( $request->all(), [
            "username" => "required|string",
            "password" => "required|string",
        ] )->validate();

        $user = User::where( "username", $request->input( "username" ) )->first();

        if ( $user == null || $user->getAuthPassword() != $user->hashPassword( $request->input( "password" ) ))
        {
            abort( 400, "Invalid username/password supplied" );
        }

        // Generate a new api token for the user and return it
        $user->generateNewToken()
            ->save();
            
        return response( $user->getApiToken() )
            ->header( "X-Expires-After", $user->getApiExpires() )
            ->header( "X-Rate-Limit", 100 );
    }

    public function logout()
    {
        abort( 501, "Not Implemented" );
    }

    public function show()
    {
        abort( 501, "Not Implemented" );
    }

    public function update()
    {
        abort( 501, "Not Implemented" );
    }

    public function destroy()
    {
        abort( 501, "Not Implemented" );        
    }

    public function store()
    {
        abort( 501, "Not Implemented" );
    }

    public function storeByArray()
    {
        abort( 501, "Not Implemented" );
    }

    public function storeByList()
    {
        abort( 501, "Not Implemented" );
    }
}
