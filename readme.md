This is a technical challenge to implement an API for the interface described [here](https://petstore.swagger.io/). This design utilises docker, follows a microservice architecture and implements two (2) endpoints.

# Building the container

1. Download and install Docker CE from [here](https://docs.docker.com/install/)
2. Download and install Docker Composer from [here](https://docs.docker.com/compose/install/)
3. Download the repository and place it into a directory. Going forward I will refer to this as the project directory.
4. Navigate **into** the project directory. This should be called Rackward-joshjumbo-hash
5. Run this command to build the image: (This might take some time the first time you use it)
	- docker-compose up --build -d
6. Once the containers have started (make sure the mysql container is running*) run:
	- docker-compose exec app bash /tmp/run.sh

When you are done using the container, you can shut them down using:
    - docker-compose down

*You can check if the mysql container is running by running 'docker ps' to view your running containers

# Accessing the service

The website is hosted on localhost so you don't have to edit your hostfile to access it. It is also only available over https.

You can use an API testing tool like [postman](https://www.getpostman.com/downloads/) to make calls to the API.

If you choose to use postman you can import the collection I have created by using [this link](https://www.getpostman.com/collections/83e52718053d2582b629).

Make sure when using Postman or similar, to turn off SSL Verification.

Otherwise, the two working endpoints are:
	- https://localhost/user/login
	- https://localhost/pet/1

# Using the service

The first call you make will have to be /user/login to get an access token. 

Use the following login credentials
**Username:** josh
**Password:** lmao

A successful  call to that endpoint will provide an access token, which you can use to plug into the api-key header for the /pet/ endpoint. 

A successful call to the /pet/ endpoint with a valid access token should return a json object of a pet. 10 random pets are seeded by default, so you should be able to access the endpoint /pet/[1-10] successfully.

# Running Tests

1. From the project directory run:
    - docker exec -it app bash /tmp/runtests.sh